#include <iostream>
#include <windows.h>
#include <conio.h>

#define FILE_MAPPING_OBJECT_NAME "file_mapping_object_name"

using namespace std;

int main()
{
	HANDLE hMapFile;
	PVOID lpMapAddress;
	char data[1024];

	hMapFile = OpenFileMappingA(FILE_MAP_ALL_ACCESS, FALSE, FILE_MAPPING_OBJECT_NAME);
	if (hMapFile != INVALID_HANDLE_VALUE) {
        cout << "File mapping opened\n";
    }
	else {
        cout << "Can't open file mapping\n";
        system("pause");
        return 1;
	}

	lpMapAddress = MapViewOfFile(hMapFile, FILE_MAP_ALL_ACCESS, 0, 0, 0);
	if (lpMapAddress == 0){
		cout << "Can't open file projection\n";
		system("pause");
		return 1;
	}
	memcpy(data, (char*)lpMapAddress, 1024);
	cout << "Address is "<< lpMapAddress <<": " << data << endl;

    cout << "Press any key to close projection and exit program";
    getch();
	UnmapViewOfFile(lpMapAddress);
	CloseHandle(hMapFile);
	return 0;
}