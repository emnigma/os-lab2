//
// Created by nigma on 28.10.2020.
//

#ifndef MAIN_CPP_VIRTUAL_ADDRES_FUNCTIONS_H
#define MAIN_CPP_VIRTUAL_ADDRES_FUNCTIONS_H

#include <iostream>
#include <windows.h>
#include <string>
#include <map>
#include <bitset>

void get_system_info_menu();
void global_memory_status_menu();
void virtual_query_menu();
void print_virtual_memory_status(DWORD address);
void reserve_virtual_memory_menu();
void reserve_and_commit_virtual_memory_menu();
void write_to_virtual_memory_menu();
void virtual_protect_menu();
void virtual_free_menu();
void free_virtual_memory(DWORD address);

#endif //MAIN_CPP_VIRTUAL_ADDRES_FUNCTIONS_H
