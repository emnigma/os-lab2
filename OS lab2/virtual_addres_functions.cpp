//
// Created by nigma on 28.10.2020.
//

#include "virtual_addres_functions.h"

#define str std::to_string
using namespace std;

typedef std::map<std::string, std::string> dict;

void get_system_info_menu() {
    SYSTEM_INFO system_info;
    GetSystemInfo(&system_info);
    std::cout << "System info:" << std::endl;
    std::cout << "Processor Architecture: ";
    switch (system_info.wProcessorArchitecture)
    {
        case PROCESSOR_ARCHITECTURE_INTEL:
            std::cout << "Intel x86" << std::endl;
            break;
        case PROCESSOR_ARCHITECTURE_ARM:
            std::cout << "ARM" << std::endl;
            break;
        case PROCESSOR_ARCHITECTURE_IA64:
            std::cout << "Intel Itanium-based" << std::endl;
            break;
        case PROCESSOR_ARCHITECTURE_AMD64:
            std::cout << "x64 (AMD or Intel)" << std::endl;
            break;
        default:
            std::cout << "Unknown architecture" << std::endl;
            break;
    }
    std::cout << "Cores: " << system_info.dwNumberOfProcessors << std::endl;
    std::cout << "Processor type: ";
    switch (system_info.dwProcessorType)
    {
        case PROCESSOR_INTEL_386:
            std::cout << "Intel 386" << std::endl;
            break;
        case PROCESSOR_INTEL_486:
            std::cout << "Intel 486" << std::endl;
            break;
        case PROCESSOR_INTEL_PENTIUM:
            std::cout << "Intel Pentium" << std::endl;
            break;
        case PROCESSOR_INTEL_IA64:
            std::cout << "Intel IA64" << std::endl;
            break;
        case PROCESSOR_AMD_X8664:
            std::cout << "AMD x8664" << std::endl;
            break;
        default:
            std::cout << "ARM" << std::endl;
            break;
    }
    std::cout << "Processor level: " << system_info.wProcessorLevel << std::endl;
    std::cout << "Page size: " << system_info.dwPageSize << std::endl;
    std::cout << "Minimum app address: " << system_info.lpMinimumApplicationAddress << std::endl;
    std::cout << "Maximum app address: " << system_info.lpMaximumApplicationAddress << std::endl;
    std::bitset<8> x(system_info.dwActiveProcessorMask);
    std::cout << "Active cores: " << x << std::endl;
}

void global_memory_status_menu() {
    MEMORYSTATUSEX statex;
    int div = 1024;
    int width = 7;

    statex.dwLength = sizeof(statex);

    GlobalMemoryStatusEx(&statex);

    printf(TEXT("There is  %*ld percent of memory in use.\n"),
              width, statex.dwMemoryLoad);
    printf(TEXT("There are %*I64d total KB of physical memory.\n"),
              width, statex.ullTotalPhys/div);
    printf(TEXT("There are %*I64d free  KB of physical memory.\n"),
              width, statex.ullAvailPhys/div);
    printf(TEXT("There are %*I64d total KB of paging file.\n"),
              width, statex.ullTotalPageFile/div);
    printf(TEXT("There are %*I64d free  KB of paging file.\n"),
              width, statex.ullAvailPageFile/div);
    printf(TEXT("There are %*I64d total KB of virtual memory.\n"),
              width, statex.ullTotalVirtual/div);
    printf(TEXT("There are %*I64d free  KB of virtual memory.\n"),
              width, statex.ullAvailVirtual/div);
    printf(TEXT("There are %*I64d free  KB of extended memory.\n"),
              width, statex.ullAvailExtendedVirtual/div);
}

void virtual_query_menu() {
    SYSTEM_INFO system_information;
    GetSystemInfo(&system_information);

    std::cout << "input address from " << system_information.lpMinimumApplicationAddress << ' ';
    cout << "to " << system_information.lpMaximumApplicationAddress << '\n';
    DWORD address;
    std::cin >> hex >> address;

    MEMORY_BASIC_INFORMATION memory_information;
    if (!VirtualQuery((LPCVOID)address, &memory_information, sizeof(memory_information))) {
        throw std::invalid_argument("VirtualQuery function failed");
    }

    print_virtual_memory_status(address);
}

void print_virtual_memory_status(DWORD address) {
    MEMORY_BASIC_INFORMATION memory_information;
    if (!VirtualQuery((LPCVOID)address, &memory_information, sizeof(memory_information))) {
        throw std::invalid_argument("VirtualQuery function failed");
    }
    cout << "Base address: " << memory_information.BaseAddress << endl;
    cout << "Allocation base: "  << memory_information.AllocationBase << endl;

    cout << "Region size: " << memory_information.RegionSize << endl;

    cout << "Page state: 0x" << hex << memory_information.State << endl;
    if (memory_information.State == MEM_COMMIT) cout << "  MEM_COMMIT" << endl;
    if (memory_information.State == MEM_FREE) cout << "  MEM_FREE" << endl;
    if (memory_information.State == MEM_RESERVE) cout << "  MEM_RESERVE" << endl;
    cout << "Page type: 0x" << hex << memory_information.Type << endl;
    if (memory_information.Type == MEM_IMAGE) cout << "  MEM_IMAGE" << endl;
    if (memory_information.Type == MEM_MAPPED) cout << "  MEM_MAPPED" << endl;
    if (memory_information.Type == MEM_PRIVATE) cout << "  MEM_PRIVATE" << endl;

    cout << "Protection: 0x" << hex << memory_information.Protect << endl;
    if (memory_information.Protect == PAGE_READONLY) cout << "  PAGE_READONLY" << endl;
    if (memory_information.Protect == PAGE_READWRITE) cout << "  PAGE_READWRITE" << endl;
    if (memory_information.Protect == PAGE_EXECUTE_WRITECOPY) cout << "  PAGE_EXECUTE_WRITECOPY" << endl;
    if (memory_information.Protect == PAGE_NOACCESS) cout << "  PAGE_NOACCESS" << endl;
    if (memory_information.Protect == PAGE_EXECUTE) cout << "  PAGE_EXECUTE" << endl;
    if (memory_information.Protect == PAGE_EXECUTE_READ) cout << "  PAGE_EXECUTE_READ" << endl;
    if (memory_information.Protect == PAGE_EXECUTE_READ) cout << "  PAGE_EXECUTE_READ" << endl;
    if (memory_information.Protect == PAGE_EXECUTE_READWRITE) cout << "  PAGE_EXECUTE_READWRITE" << endl;
    if (memory_information.Protect == PAGE_EXECUTE_WRITECOPY) cout << "  PAGE_EXECUTE_WRITECOPY" << endl;
    if (memory_information.Protect == PAGE_GUARD) cout << "  PAGE_GUARD" << endl;
    if (memory_information.Protect == PAGE_NOCACHE) cout << "  PAGE_NOCACHE" << endl;
    if (memory_information.Protect == PAGE_WRITECOMBINE) cout << "  PAGE_WRITECOMBINE" << endl;
}

void reserve_virtual_memory_menu() {
    PVOID pMemory = 0;
    PVOID pBaseAddress = 0;
    DWORD dwSize = 0;
    cout << "Base address (0 for auto): 0x";
    cin >> hex >> pBaseAddress;

    cout << "Size (in bytes): ";
    cin >> dec >> dwSize;

    pMemory = VirtualAlloc(pBaseAddress, dwSize, MEM_RESERVE, PAGE_READWRITE);
    if (pMemory != NULL) {
        cout << "\n\nMemory reserved\n";
        print_virtual_memory_status((DWORD)pMemory);
    }
    else {
        cout << "Memory reservation error occurred!" << endl;
        return;
    }
}

void reserve_and_commit_virtual_memory_menu() {
    PVOID pMemory = 0;
    PVOID pBaseAddress = 0;
    DWORD dwSize = 0;
    cout << "Base address (0 for auto): 0x";
    cin >> hex >> pBaseAddress;

    cout << "Size (in bytes): ";
    cin >> dec >> dwSize;

    pMemory = VirtualAlloc(pBaseAddress, dwSize, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    if (pMemory != NULL) {
        cout << "\n\nMemory committed\n";
        print_virtual_memory_status((DWORD)pMemory);
    }
    else {
        cout << "Cant reserve or commit memory!" << endl;
        return;
    }
}

void write_to_virtual_memory_menu() {
    PVOID pMemory = 0;
    PVOID pBaseAddress = 0;
    int data = 0;
    printf("Base address (0 for auto): 0x");
    cin >> hex >> pBaseAddress;

    cout << "Input data: ";
    cin >> dec >> data;

    if (pBaseAddress) {
        pMemory = VirtualAlloc(pBaseAddress, sizeof(data), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
        cout << "Memory allocated\n";
        memcpy(pMemory, &data, sizeof(data));
        cout << "Memory read (" << hex << pBaseAddress << dec << "): " << (*(PDWORD)pBaseAddress) << endl;
    }
    else {
        pMemory = VirtualAlloc(pBaseAddress, sizeof(data), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
        if (pMemory != NULL) {
            cout << "Memory allocated\n";
            memcpy(pMemory, &data, sizeof(data));
            cout << "Memory read (" << hex << pMemory << dec << "): " << (*(PDWORD)pMemory) << endl;
        }
        else {
            cout << "Cant allocate memory!" << endl;
            cout << GetLastError() << endl;
            return;
        }
    }
}

void virtual_protect_menu() {
    PVOID pMemory = 0;
    PVOID pBaseAddress = 0;
    DWORD dwSize = 0;
    cout << "Base address (0 for auto): 0x";
    cin >> hex >> pBaseAddress;

    if (pBaseAddress) {
        DWORD flOldProtect = 0;
        VirtualProtect(pBaseAddress, sizeof(int), PAGE_NOACCESS, &flOldProtect);
        cout << "\n\nMemory protected:\n";
        print_virtual_memory_status((DWORD)pBaseAddress);
    }
    else {
        pMemory = VirtualAlloc(pBaseAddress, sizeof(int), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
        if (pMemory != NULL) {
            cout << "\n\nMemory committed:\n";
            print_virtual_memory_status((DWORD)pMemory);
        }

        DWORD flOldProtect = 0;
        VirtualProtect(pMemory, sizeof(int), PAGE_NOACCESS, &flOldProtect);
        cout << "\n\nMemory protected:\n";
        print_virtual_memory_status((DWORD)pMemory);

        VirtualFree(pMemory, 0, MEM_RELEASE);
    }
}

void virtual_free_menu() {

    SYSTEM_INFO system_information;
    GetSystemInfo(&system_information);

    std::cout << "input address from " << system_information.lpMinimumApplicationAddress << ' ';
    cout << "to " << system_information.lpMaximumApplicationAddress << '\n';
    DWORD address;
    std::cin >> hex >> address;

    free_virtual_memory(address);
}

void free_virtual_memory(DWORD address) {
    VirtualFree((LPVOID)address, 0, MEM_RELEASE);
    cout << "\n\nMemory freed:\n";
    print_virtual_memory_status((DWORD)address);
}