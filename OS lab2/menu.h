//
// Created by nigma on 28.10.2020.
//

#ifndef MAIN_CPP_MENU_H
#define MAIN_CPP_MENU_H

#define print std::cout
#define input std::cin

#include <conio.h>
#include "virtual_addres_functions.h"

void show_menu_message() {
    print << "Choose menu option:\n";
    print << "0 - System info\n";
    print << "1 - Memory status\n";
    print << "2 - Virtual memory status\n";
    print << "3 - Reserve region\n";
    print << "4 - Reserve and commit region\n";
    print << "5 - Write data\n";
    print << "6 - Protect region\n";
    print << "7 - Free memory\n";
    print << "8 - Exit\n";
}

void show_menu() {
    system("cls");
    int menu_option = -1;
    while (menu_option != 8) {
        show_menu_message();
        input >> menu_option;
        switch (menu_option) {
            case 0:
                get_system_info_menu();
                break;
            case 1:
                global_memory_status_menu();
                break;
            case 2:
                virtual_query_menu();
                break;
            case 3:
                reserve_virtual_memory_menu();
                break;
            case 4:
                reserve_and_commit_virtual_memory_menu();
                break;
            case 5:
                write_to_virtual_memory_menu();
                break;
            case 6:
                virtual_protect_menu();
                break;
            case 7:
                virtual_free_menu();
                break;
            case 8:
                print << "exit";
                return;
            default:
                break;
        }
        print << "press any button to continue...\n";
        getch();
        system("cls");
    }
}

#endif //MAIN_CPP_MENU_H
