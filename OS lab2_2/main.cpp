#include <iostream>
#include <windows.h>
#include <conio.h>

#define FILE_MAPPING_OBJECT_NAME "file_mapping_object_name"

using namespace std;

int main()
{
    HANDLE hFile;
    HANDLE hMapFile;
    LPVOID lpMapAddress;
    const char* filename = "temporary_file_lab2.txt";
    char data[1024];
    cout << "Data for transmitting: ";
    cin >> data;

    hFile = CreateFile(filename, GENERIC_ALL, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    if (hMapFile != INVALID_HANDLE_VALUE) {
        cout << "File created successfully\n";
    }
    else {
        cout << "Can't create file\n";
        system("pause");
        return 1;
    }

    hMapFile = CreateFileMappingA(hFile, NULL, PAGE_READWRITE, 0, 1024, FILE_MAPPING_OBJECT_NAME);

    if (hMapFile != INVALID_HANDLE_VALUE) {
        cout << "Object mapping created\n";
    }

    lpMapAddress = MapViewOfFile(hMapFile, FILE_MAP_ALL_ACCESS, 0, 0, 0);

    memcpy(lpMapAddress, data, strlen(data));
    cout << "Projection address: " << lpMapAddress << endl;

    cout << "Press any key to close projection and exit program";
    getch();
    UnmapViewOfFile(lpMapAddress);
    CloseHandle(hMapFile);
    CloseHandle(hFile);
    return 0;
}